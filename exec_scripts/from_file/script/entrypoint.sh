#!/bin/sh
# Setup script for enfless loop
echo starting

DIR="/tmp"
echo "My Holzmichel" > ${DIR}/busy-test.log

i=1
count=10
while [ $i -le $count ]
do
  echo "$i of $count"
  i=$(( $i + 1 ))
  echo "My Holzi " `date` >> ${DIR}/test.log
  sleep 2
done