# MongoDB

### Bash-Login

```bash
mongo -u mongo -p mongo --authenticationDatabase mongodb
```

# MongoDB Compass

### Fields

Hostname: 127.0.0.1

Port: 27017

SRV false

Authentication: User/Password

Username: mongo

Password: mongo

Authentication Database: mongodb

### Connection-String

mongodb://mongo:mongo@127.0.0.1:27017/?authSource=mongodb
