# Treafik as Loadbalancer

### Run traefik

``` bash
docker-compose -f docker-compose-traefik.yml up -d
```

Under `localhost:8080` you shoud see the web-ui.

### Run a service 

``` bash
docker-compose -f docker-compose-whoami.yaml up -d
```

Under `localhost:8080` you shoud find your service whoami now.

Note that we have not expose a port in docker-compose-whoami.yaml!!!

Insert http://whoami.localhost/ in browser and get for example:

```
Hostname: d7f919e54651
IP: 127.0.0.1
IP: 192.168.64.2
GET / HTTP/1.1
Host: whoami.localhost
User-Agent: curl/7.52.1
Accept: */*
Accept-Encoding: gzip
X-Forwarded-For: 192.168.64.1
X-Forwarded-Host: whoami.localhost
X-Forwarded-Port: 80
X-Forwarded-Proto: http
X-Forwarded-Server: 7f0c797dbc51
X-Real-Ip: 192.168.64.1
```
